from typing import Optional
import requests
from requests.exceptions import HTTPError


class NotFound(HTTPError):
    pass


class HTTPClient:
    def request(self, url, method, payload: Optional[dict] = None):
        requests_payload = dict()
        if payload is not None:
            requests_payload['json'] = payload

        response = requests.request(
            method=method,
            url=url,
            **requests_payload,
        )

        if response.status_code == 404:
            raise NotFound()

        return response.json()

    def get(self, url: str, *args, **kwargs):
        return self.request(url, method='GET', *args, **kwargs)

    def post(self, url: str, payload: dict, *args, **kwargs):
        return self.request(url, method='POST', payload=payload, *args, **kwargs)

    def delete(self, url: str, *args, **kwargs):
        return self.request(url, method='DELETE', *args, **kwargs)
