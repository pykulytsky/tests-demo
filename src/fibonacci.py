from dataclasses import dataclass


@dataclass
class Fibonacci:
    num: int

    def execute(self):
        a, b = 0,1
        while a <= self.num:
            yield a
            a, b = b, a + b
