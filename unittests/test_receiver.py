import unittest
from src.receiver import Receiver


class ReceiverTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.receiver = Receiver("Oleh", "Pykulytsky", "pykulytsky@gmail.com")

    def test_receiver(self):
        self.assertIn("email", self.receiver.to_json())
        self.assertIsInstance(self.receiver.to_json(), dict)
        self.assertEqual(self.receiver.to_json()["name"], "Oleh Pykulytsky")
