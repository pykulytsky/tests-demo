import unittest
from src.http import HTTPClient, NotFound


class HttpClientTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.client = HTTPClient()

    def test_not_found(self):
        with self.assertRaises(NotFound):
            self.client.get("https://www.python.org/wrong/endpoint")
