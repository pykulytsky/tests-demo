from src.fibonacci import Fibonacci


def test_fibonacci():
    fibonacci = Fibonacci(89)

    fibonacci_seq = [i for i in fibonacci.execute()]

    assert fibonacci_seq[-1] == 89
    assert all([
        True if fibonacci_seq[i-2] + fibonacci_seq[i-1] == fibonacci_seq[i]
        else False
        for i in range(len(fibonacci_seq)-1, 1, -1)]
    )
