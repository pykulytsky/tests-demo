import pytest
from src.http import NotFound, HTTPClient


def test_not_found():
    client = HTTPClient()

    with pytest.raises(NotFound):
        client.get("https://www.python.org/wrong/endpoint")
