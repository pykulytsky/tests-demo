from datetime import datetime
import pytest
from src.receiver import Receiver
from django.contrib.auth.models import User


pytestmark = [pytest.mark.django_db]


def authenticate(username, password):
    user = User.objects.get(username=username)
    user.last_login = datetime.utcnow()
    user.save()

    return user


def test_receiver():
    receiver = Receiver("Oleh", "Pykulytsky", "pykulytsky@gmail.com")

    assert "email" in receiver.to_json()
    assert isinstance(receiver.to_json(), dict)
    assert receiver.to_json()["name"] == "Oleh Pykulytsky"


def test_receiver_from_user_model(user):
    receiver = Receiver.from_user_model(user)

    assert "email" in receiver.to_json()
    assert isinstance(receiver.to_json(), dict)
    assert receiver.to_json()["name"] == "Oleh Pykulytsky"


@pytest.mark.freeze_time
def test_user_last_login(user):
    logined_user = authenticate(username=user.username, password="123456")

    assert logined_user.last_login is not None
    assert logined_user.last_login == datetime.now()
