import factory
import pytest
from django.contrib.auth.models import User
from mixer.backend.django import mixer
from pytest_factoryboy import register


@pytest.fixture
def user():
    return User.objects.create_user(
        username="pykulytsky",
        password="123456",
        email="pykulytsky@gmail.com",
        first_name="Oleh",
        last_name="Pykulytsky"
    )


@pytest.fixture
def another_user():
    return mixer.blend(User)


class AuthorFactory(factory.django.DjangoModelFactory):
    username = "test"
    password = "123456"
    email = "test@gmail.com"
    first_name = "Oleh"
    last_name = "Pykulytsky"

    class Meta:
        model = User


@pytest.fixture
def author():
    return AuthorFactory()


register(AuthorFactory)
